<?php

namespace App\Console\Commands;

use App\Models\DaemonStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Daemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daemon';

    protected bool $is_run=true;

    protected int $pause_length = 1*60;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daemon manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $status=DaemonStatus::latest()->first()?->is_run;
        $question='Now daemon status is'.($status?'':' not').' run. Wish you start or stop daemon?';
        $command = $this->anticipate($question, ['start', 'stop']);
        $command = $command === 'stop' ? 'stop' : 'start';
        DaemonStatus::create(['is_run' => $command !== 'stop']);
        if ($command === 'start')
            $this->init();
        else
            print_r('Daemon stopped' . PHP_EOL);
    }

    private function init()
    {
        /**
         * pcntl_fork() - данная функция разветвляет текущий процесс
         */

        $pid = pcntl_fork();
        if ($pid == -1) {
            die('Error fork process' . PHP_EOL);
        } elseif ($pid) {
            /**
             * В эту ветку зайдет только родительский процесс, который мы убиваем и сообщаем об этом в консоль
             */
            die('Daemon started' . PHP_EOL);
        } else {
            /**
             * Цикл с паузой
             */
            while ($this->is_run) {
                Artisan::call('schedule:run');
                $this->is_run = DaemonStatus::latest()->first()->is_run;
                sleep($this->pause_length);
            }
        }
        /**
         * Установим дочерний процесс основным, это необходимо для создания процессов
         */
        posix_setsid();
    }
}
