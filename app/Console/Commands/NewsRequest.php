<?php

namespace App\Console\Commands;

use App\Models\News;
use App\Models\NewsRequests;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use XMLReader;
use SimpleXMLElement;
use Illuminate\Console\Command;

class NewsRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $client = new Client();
        $request = new Request('GET', 'http://static.feed.rbc.ru/rbc/logical/footer/news.rss');
        $response = $client->send($request);
        $response_status_code = $response->getStatusCode();
        $response_body = $response->getBody();

        NewsRequests::create([
            'request_method' => $request->getMethod(),
            'request_uri' => $request->getUri(),
            'response_status_code' => $response_status_code,
            'response_body' => $response_body,
        ]);

        if ($response_status_code === 200) {
            $this->parseRss($response_body);
        }
    }

    private function parseRss($data)
    {
        $reader = new XMLReader();
        $reader->XML($data);

        while ($reader->read() && $reader->name !== 'item') ;

        while ($reader->name === 'item') {
            $xml = new SimpleXMLElement($reader->readOuterXML());

            News::updateOrCreate([
                'rbc_id' => (string)$xml->xpath('//rbc_news:news_id')[0],
            ], [
                    'title' => (string)$xml->title,
                    'link' => (string)$xml->link,
                    'description' => (string)$xml->description,
                    'published_at' => (string)$xml->pubDate,
                    'author' => (string)$xml->author,
                    'img_url' => $xml->enclosure ? (string)$xml->enclosure[0]->attributes()->url : '',
                ]

            );
            $reader->next('item');
        }

        return $reader->close();
    }
}
