<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsRequests extends Model
{
    protected $fillable = [
        'request_method',
        'request_uri',
        'response_status_code',
        'response_body',
    ];
}
