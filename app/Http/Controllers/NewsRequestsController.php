<?php

namespace App\Http\Controllers;

use App\Models\NewsRequests;

class NewsRequestsController extends Controller
{
    public function index()
    {
        return view('news_requests.index', [
            'title' => 'news',
            'items' => NewsRequests::get(),
        ]);
    }

}
