<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'rbc_id' => $this->faker->md5(),
            'title' => $this->faker->sentence(6, true),
            'link' => $this->faker->url(),
            'description' => $this->faker->realText(),
            'published_at' => $this->faker->dateTime(),
            'author' => $this->faker->name(),
            'img_url' => $this->faker->url(),
        ];
    }
}
