##Использование

```bash
php artisan daemon
```

##Разворачивание проекта

1. На сервере переходим в директорию, где будет развернут проект, например:

```bash
cd /var/www
```

2. Клонируем проект из репозитория, например:

```bash
git clone https://PaulLebedev@bitbucket.org/PaulLebedev/console-news.git
```
3. Переходим в корневую директорию проекта, например:

```bash
cd console-news
```
4. Создаем конфигурационный файл проекта копируя .env.example

```bash
cp .env.example .env
```
5. Редактируем конфигурационный файл добавив в него доступы к БД,
   устанавливаем переменные окружения в production (APP_ENV=production)
   и отключаем режим отображения ошибок (APP_DEBUG=false)


6. Выдаем веб-серверу права на нужные директории
```bash
chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
```

7. Запускаем composer
```bash
composer install
```
8. Запускаем миграции и сидирование БД

```bash
php artisan migrate --seed
```
9. Конфигурируем веб-сервер для перенаправления всех запросов на файл index.php в поддиректории проекта 'public'


10. Прописываем вызов приложения в crontab

###Некоторые комманды, которые могут оказаться полезны
Если ключ приложеня не сгенерировался автоматически
```bash
php artisan key:generate
```
Если нужно повторно мигрировать БД

```bash
php artisan migrate:fresh --seed
```
