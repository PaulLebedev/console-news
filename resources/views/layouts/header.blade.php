<header class="d-flex justify-content-center py-3">
    <ul class="nav nav-pills">
        @foreach([
    ['route_name'=>'news.index','text'=>'News'],
    ['route_name'=>'news_requests.index','text'=>'News requests'],
    ] as $menu_item)
            <li class="nav-item"><a href="{{route($menu_item['route_name'])}}"
                                    class="nav-link{{\Illuminate\Support\Facades\Route::currentRouteName()===$menu_item['route_name']?' active':''}}"
                                    aria-current="page">{{$menu_item['text']}}</a></li>
        @endforeach
    </ul>
</header>
