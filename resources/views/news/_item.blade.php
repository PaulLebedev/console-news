<div class="col">
    <div class="card shadow-sm">
        @if($item->img_url && strripos($item->img_url,'.jpg'))
            <img src="{{$item->img_url}}"  width="100%" height="225">
        @else
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                 xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail"
                 preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>
                <rect width="100%" height="100%" fill="#55595c"></rect>
            </svg>
        @endif
        <div class="card-body">
            <p class="card-text">{{$item->title}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <small class="text-muted">{{$item->author}}</small>
                <small class="text-muted">{{$item->published_at}}</small>
            </div>
        </div>
    </div>
</div>
