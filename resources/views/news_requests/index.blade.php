@extends('layouts.app')

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Method</th>
                <th scope="col">Url</th>
                <th scope="col">Code</th>
                <th scope="col">Body</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                @include('news_requests._item')
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
