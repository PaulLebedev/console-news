<tr>
    <td>{{$item->id}}</td>
    <td>{{$item->request_method}}</td>
    <td>{{$item->request_uri}}</td>
    <td>{{$item->response_status_code}}</td>
    <td><p style="
  width: 500px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;" title="{{$item->response_body}}">{{$item->response_body}}</p></td>
    <td>{{$item->created_at}}</td>
</tr>
